const header = document.querySelector('nav');
const sectionOne = document.querySelector('.dashboards');
const NavLinks = document.querySelector('li');
const sectionOneOptions = {
  rootMargin: '-200px 0px 0px 0px',
};

const sectionOneObserver = new IntersectionObserver(
  function (entries, sectionOneObserver) {
    entries.forEach((entry) => {
      if (!entry.isIntersecting) {
        NavLinks.classList.add('nav-links-scrolled');
        header.classList.add('nav-scrolled');
      } else {
        header.classList.remove('nav-scrolled');
        NavLinks.classList.remove('nav-links-scrolled');
      }
    });
  },
  sectionOneOptions
);

sectionOneObserver.observe(sectionOne, NavLinks);

$('.flexslider').flexslider({
  animation: 'slide',
  controlNav: false,
});
